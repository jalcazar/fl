# Exercises from _Software Foundations Vol 1: Logical Foundations_

https://softwarefoundations.cis.upenn.edu/lf-current/toc.html
Aug 13th 2018.

MD5 (lf.tgz) = 07b2a38ef4cd9ab359b135f8248c1af3


Using Coq 8.7.1.
```
$ coqc -v
The Coq Proof Assistant, version 8.7.1 (December 2017)
compiled on Dec 17 2017 1:23:04 with OCaml 4.05.0
```