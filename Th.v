
Variables A B C : Prop.
Lemma distr_impl : (A -> B -> C) -> (A -> B) -> A -> C.

Proof.
  intro H.
  intros H' HA.
  apply H.
    exact HA.
    apply H'.
    assumption.
Qed.

