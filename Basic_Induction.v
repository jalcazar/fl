Require Export Basics.


Theorem mult_0_r : forall n:nat,
  n * 0 = 0.
Proof.
(intros n).
(induction n as [| n' IHn']).
 (simpl).
 reflexivity.

 (simpl).
 (rewrite IHn').
 reflexivity.

Qed.

(* GRADE_THEOREM 0.5: mult_0_r *)
Theorem plus_n_Sm : forall n m : nat,
  S (n + m) = n + (S m).
Proof.
(intros n m).
(induction n as [| n' IHn']).
 (simpl).
 reflexivity.

 (simpl).
 (rewrite IHn').
 reflexivity.

Qed.
(* GRADE_THEOREM 0.5: plus_n_Sm *)
Lemma plus_n_r : forall n:nat, n + 0 = n.
Proof.
  intros n.
  induction n as [| r'].
  reflexivity.
  simpl.
  rewrite -> IHr'.
  reflexivity.
Qed.

Lemma plus_dist : forall j k : nat, S (j + k) = j + S (k).
Proof.
  intros j k.
  induction j as [| j' IHj'].
  reflexivity.
  simpl.
  rewrite -> IHj'.
  reflexivity.
Qed.

Theorem plus_comm : forall n m : nat,
  n + m = m + n.
Proof.
  intros n m.
  induction n as [| n' IHn'].
  simpl.
  rewrite -> plus_n_r.
  reflexivity.
  simpl.
  rewrite ->IHn'.
  rewrite -> plus_dist.
  reflexivity.
Qed.

(* GRADE_THEOREM 0.5: plus_comm *)
Theorem plus_assoc : forall n m p : nat,
  n + (m + p) = (n + m) + p.
Proof.
  intros n m p.
  induction n as [| n' IHn'].
  reflexivity.
  simpl.
  rewrite -> IHn'.
  reflexivity.
Qed.

(* GRADE_THEOREM 0.5: plus_assoc *)

