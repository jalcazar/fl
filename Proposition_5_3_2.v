(*
from the book "Discrete Mathematics with Applications" by Susanna S. Epp
Proposition 5.3.2

For all integers n ≥ 3, 2n + 1 < 2^n
*)


Definition two_n_plus_one (n : nat) : nat
  :=
    2 * n + 1.

Fixpoint exp (base power : nat) : nat :=
  match power with
    | O => S O
    | S p => mult base (exp base p)
  end.

Fixpoint ble_nat (n m : nat) : bool
  :=
    match n with
    | O => true
    | S n' => match m with
              | O => false
              | S m' => ble_nat n' m'
              end
    end.

Fixpoint bge_nat (n m : nat) : bool
  :=
    match n with
    | O => true
    | S n' => match m with
              | O => true
              | S m' => bge_nat n' m'
              end
    end.

Fixpoint beq_nat (n m : nat) : bool :=
  match n with
  | O => match m with
         | O => true
         | S m' => false
         end
  | S n' => match m with
            | O => false
            | S m' => beq_nat n' m'
            end
  end.

Fixpoint leb (n m : nat) : bool :=
  match n with
  | O => true
  | S n' =>
      match m with
      | O => false
      | S m' => leb n' m'
      end
  end.


Definition blt_nat (n m : nat) : bool
  :=
    match n with
    | nat => (andb (leb n m) (negb (beq_nat n m)))
    end.


(**
Theorem two_n_plus_one_geq_three_lt_two_pow_n :
  forall n : nat, two_n_plus_one n <=  exp 2 n -> n>= 3.
*)

(* n ≥ 3, 2n + 1 < 2^n *)
Theorem two_n_plus_one_leq_three_lt_wo_pow_n : forall n:nat,
    (blt_nat (two_n_plus_one n) (exp 2 n)) = true
       -> (bge_nat n 3) = true.
Proof.
  intros n.
  destruct n.
  (* n = 0 *)
  compute.
  intros HF. (* Discard the cases where n is not >= 3 *)
  inversion HF.

  destruct n.
  (* n = 1 *)
  compute.
  intros HF.
  inversion HF.

  destruct n.
  (* n = 2 *)
  compute.
  intros HF.
  inversion HF.

  induction n as [ | k IHk].
  (* n = 3 *)
  - compute.
    reflexivity.
  (* n+1 inductive step *)
  - generalize dependent k.
    intros.
    compute.
    reflexivity.
Qed.


Example zero_two_n_plus_one: (two_n_plus_one 0) = 1.
Proof. simpl. reflexivity. Qed.

Example one_two_n_plus_one: (two_n_plus_one 1) = 3.
Proof. simpl. reflexivity. Qed.

Example one_two_n_plus_three: (two_n_plus_one 3) = 7.
Proof. simpl. reflexivity. Qed.

Example one_two_n_plus_ten: (two_n_plus_one 10) = 21.
Proof. simpl. reflexivity. Qed.


