Theorem plus_id_exercise : forall n m o: nat,
 n = m -> m = o -> n + m = m + o.

Proof.
 intros n m o.
 intros H0.
 intros H1.
 rewrite -> H0.
 rewrite <- H1.
 reflexivity.
Qed.
(* This proof doesn't fail but how to know whether it is valid *)
