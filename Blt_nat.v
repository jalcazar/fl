
Definition andb (b1:bool) (b2:bool) : bool :=
  match b1 with
  | true => b2
  | false => false
  end.

Definition negb (b:bool) : bool :=
  match b with
  | true => false
  | false => true
  end.

Fixpoint beq_nat (n m : nat) : bool :=
  match n with
  | O => match m with
         | O => true
         | S m' => false
         end
  | S n' => match m with
            | O => false
            | S m' => beq_nat n' m'
            end
  end.

Fixpoint leb (n m : nat) : bool :=
  match n with
  | O => true
  | S n' =>
      match m with
      | O => false
      | S m' => leb n' m'
      end
  end.



Definition blt_nat (n m : nat) : bool
:=
  match n with
  | nat => (andb (leb n m) (negb (beq_nat n m)))
  end.

Example test_blt_nat1:             (blt_nat 2 2) = false.
Proof. reflexivity. Qed.

Example test_blt_nat2:             (blt_nat 2 4) = true.
Proof. reflexivity. Qed.

Example test_blt_nat3:             (blt_nat 4 2) = false.
Proof. reflexivity. Qed.

