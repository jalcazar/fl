Definition minusthree (n : nat) : nat :=
  match n with
    | O => O
    | S O => O
    | S (S O) => O
    | S (S (S n')) => n'
  end.
Compute (minusthree 99).
Compute (minusthree 4).
Compute (minusthree 9).

